<?php
/**
 * Created by PhpStorm.
 * User: sbwdlihao
 * Date: 12/29/15
 * Time: 3:40 PM
 */
namespace Numbull\Warning\Log\Mongo;

require_once __DIR__.'/../vendor/autoload.php';

use Numbull\Warning\Plugin\PluginImp;

class Plugin extends PluginImp{

    public function __construct($id = 0)
    {
        parent::__construct($id);
    }

    public function getType()
    {
        return 'log';
    }

    public function getName()
    {
        return 'warning_log_mongo';
    }

    public function getDescription()
    {
        return '监控日志';
    }

    public function showConfig()
    {
        return [
            'host'=>'数据库服务器地址',
            'port'=>'数据库服务器端口',
            'username'=>'用户名,没有则填充-',
            'password'=>"密码,没有则填充-",
            'database'=>'数据库名称',
            'table'=>'表名称',
            'header_type'=>'查询删选条件header的type属性值',
            'header_hostname'=>'查询删选条件header的hostname属性值',
            'error_line_num'=>'当天错误日志的行数阈值,json字符串,比如{"low":1,"middle":2,"high":3}',
        ];
    }

    public function action($params)
    {
        if (count($params) != 9) {
            $this->_showResult(201, json_encode($params));
        }
        $host = $params[0];
        $port = $params[1];
        $username = $params[2];
        $password = $params[3];
        $database = $params[4];
        $table = $params[5];
        $header_type = $params[6];
        $header_hostname = $params[7];
        $error_line_num = json_decode($params[8], true);

        $result = [];
        $server = sprintf('mongodb://%s:%s', $host, $port);
        $options['db'] = 'admin';
        $options['connectTimeoutMS'] = 5000;
        if ($username != '-' && $password != '-') {
            $options['username'] = $username;
            $options['password'] = $password;
        }
        try {
            $m = new \MongoClient($server, $options);
        } catch(\MongoConnectionException $e) {
            $this->_showResult(201, sprintf('Mongo db connect exception = %s', $e->getMessage()));
        }
        try {
            $db = $m->selectDB($database);
        } catch(Exception $e) {
            $this->_showResult(201, sprintf('Mongo db select database exception = %s', $e->getMessage()));
        }
        try {
            $tb = $db->selectCollection($table);
        } catch(Exception $e) {
            $this->_showResult(201, sprintf('Mongo db select table exception = %s', $e->getMessage()));
        }

        try {
            // 统计错误的日志条数
            $today_zero = mktime(0, 0, 0, date('m'), date('d'), date('y'));
            $count = $tb->count([
                'header'=>['type'=>$header_type, 'hostname'=>$header_hostname],
                't_n'=>['$gt'=>''.$today_zero],
                'V1'=>['$regex'=>'ERROR'],
            ]);
            if ($count > $error_line_num['high']) {
                $result['error_line_num'][0]['level'] = 'high';
                $result['error_line_num'][0]['message'] = sprintf('log error line num is too large, set = %s, real = %s', $error_line_num, $count);
            } elseif ($count > $error_line_num['middle']) {
                $result['error_line_num'][0]['level'] = 'middle';
                $result['error_line_num'][0]['message'] = sprintf('log error line num is too large, set = %s, real = %s', $error_line_num, $count);
            } elseif ($count > $error_line_num['low']) {
                $result['error_line_num'][0]['level'] = 'low';
                $result['error_line_num'][0]['message'] = sprintf('log error line num is too large, set = %s, real = %s', $error_line_num, $count);
            }
        } catch(Exception $e) {
            $this->_showResult(201, $e->getMessage());
        }

        if (!empty($result)) {
            $this->_showResult(301, json_encode($result));
        }

        parent::action($params);
    }
}